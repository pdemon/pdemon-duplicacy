# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.0-rc4

**Features**
* Puppet 6.x support
* Created types to represent the massive repository data structure simplifying use & code
* Reworked prune entries to allow pruning other repos with the same storage from within a given repository

**Bugfixes**
* Prune report argument is now correct

## Release 0.1.0-rc3

**Features**
* Added a log summary function so enormous logs aren't emailed all the time

**Bugfixes**
* Fixed some potentially bad syntax in the backup and prune scripts.

**Known Issues**
