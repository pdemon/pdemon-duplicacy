type Duplicacy::EmailRecipient = Pattern[/[[:alnum:]._%+-]+@[[:alnum:].-]+\.[[:alnum:]]{2,}/]
