type Duplicacy::ScheduleEntry = Hash[
  String,
  Variant[
    String,
    Integer,
    Array,
  ],
]
