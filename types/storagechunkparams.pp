type Duplicacy::StorageChunkParams = Hash[
  Enum[
    'size',
    'max',
    'min',
  ],
  Integer,
]
